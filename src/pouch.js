import Vue from 'vue'
import PouchDB from 'pouchdb'
import PouchFind from 'pouchdb-find'
import test_data from './test_data'
import eve from './event'

PouchDB.plugin(PouchFind)

//PouchDB.debug.enable('*');
PouchDB.debug.disable();
//let db = new PouchDB('http://localhost:5984/demo/')

// обработать потерю соединения


export default {
  async open(){
    let db = new PouchDB('local')
    try{
      await db.destroy()
    }
    catch (e){}
    db = new PouchDB('local')
    let info = await db.info()

    db.changes({
      since: 'now',
      live: true,
      include_docs: true
    }).on('change', function (change) {
      // change.id contains the doc id, change.doc contains the doc
      if (change.deleted) {
        // document was deleted
      } else {
        // document was added/modified
      }
    }).on('error', function (err) {
      // handle errors
      console.log(err)
    });

    // установка тестовых данных
    await db.bulkDocs(test_data);
    Vue.prototype.$db = db
    eve.db.opened()
  },
  close(){
    eve.db.closed()
  }
}
