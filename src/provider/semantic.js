import con from '../connection'

let sub = null;
export default {
  init(){
    this.theme.parent = this
    this.phrase.parent = this
    this.request.parent = this
    delete this.init
    return this
  },
  get_destination(){
    return "semantic"
  },

  subscribe(){
    // sub = con.subscribe(this._events_destination, (m)=>{
    //   if(m.context === 'doc_document'){
    //     if(m.type === 'created')
    //       store.dispatch(name.soz.e.order_created, m.body);
    //     else if(m.type === 'updated')
    //       store.dispatch(name.soz.e.order_updated, m.body);
    //   }
    //   else if(m.context === 'doc_processing'){
    //     store.dispatch(name.soz.e.stages_updated, m.body);
    //   }
    //   else if(m.context === 'doc_attribute') {
    //     store.dispatch(name.soz.e.attributes_updated, m.body);
    //   }
    // });
  },
  unsubscribe(){
    // if(sub !== null)
    //   sub.unsubscribe();
    // sub = null;
  },

  theme:{
    // orders(prefix, type, string1, state, limit, int5, create_by, identifiers, from, to){
    //   return con.request(this.parent.get_destination(prefix), '/order/orders', {type, limit, string1, state, int5, create_by, identifiers, from, to})
    // },
    get(){
      return con.request(this.parent.get_destination(), '/theme/get', {})
    },
  },
  request:{
    get(theme_id){
      return con.request(this.parent.get_destination(), '/request/get', {theme_id})
    },
    delete(ids){
      return con.request(this.parent.get_destination(), '/request/delete', {ids})
    }
  },
  phrase:{
    phrases(theme_id){
      return con.request(this.parent.get_destination(), '/phrase/phrases', {theme_id})
    },
    set_target(ids, is_target){
      return con.request(this.parent.get_destination(), '/phrase/set_target', {ids, is_target})
    }
  },



  // card(args){
  //   /*
  //     {
  //       type: 26,
  //       id: 3088,
  //       status: "all", // {all, active, blocked}
  //       tariff_plan: "Базовый",
  //       tariff: "Базовый Автокарта",
  //       currency: "RUB",
  //       card: "PayPass MasterCard World",
  //       design: "Море и горы",
  //       card_param: "",
  //       action: "3 месяца бесплатного обслуживания",
  //       segment: "Нет",
  //       sys_code: "5103",
  //       sys_group: "183"
  //     }
  //   */
  //   return con.get(this._destination, "product", "card", JSON.stringify(args));
  // },
  // credit(args){
  //   /*
  //   {
  //     sys_code: "3569",
  //     id: 108
  //   }
  //   */
  //   return con.get(this._destination, "product", "credit", JSON.stringify(args));
  // },
  // card_tariff(type){
  //   return con.get(this._destination, "product", "tariff", JSON.stringify({type}));
  // },
  // get_filtered_orders(args){
  //   /*
  //    {
  //      "limit": 10,
  //      "create_by": 123
  //      "type": 26,
  //      "string1": "asd"
  //    }
  //   * */
  //   return con.get(this._destination, "order", "filtered", JSON.stringify(args));
  // },
  // get_orders(orders){
  //   /*
  //   {
  //       "identifiers": [12123, 12123123]
  //   }
  //   */
  //   return con.get(this._destination, "order", "", JSON.stringify({identifiers: orders}));
  // },
  // get_attributes(args){
  //   /*
  //    {
  //       "order_id": 12123,
  //       "attributes": ["attr1", "attr2"]
  //    }
  //    */
  //   return con.get(this._destination, "order", "attributes", JSON.stringify(args));
  // },
  // get_stages(args){
  //   /*
  //    {
  //       "order_id": 12123,
  //       "full": true
  //    }
  //    */
  //   return con.get(this._destination, "order", "stages", JSON.stringify(args));
  // },
  //
  // save_order(attrs){
  //   let req = { attributes: attrs };
  //   return con.post(this._destination, "order", "", JSON.stringify(req));
  // },
  // save_stage(attrs){
  //   let req = { attributes: attrs }
  //   return con.post(this._destination, "order", "stage", JSON.stringify(req));
  // },
  // get_users(logins){
  //   return con.get(this._destination, "user", "", JSON.stringify({logins}));
  // },
  // save_user(login, role, firstname, lastname, middlename, description){
  //   return con.post(this._destination, "user", "save", JSON.stringify({login, role, firstname, lastname, middlename, description}))
  // },
  // get_roles(){
  //   return con.get(this._destination, "catalog", "role", '');
  // }
}.init()
