import con from '../../connection'
import db from '../../db'

export default {
  init(){
    this.soz_user.parent = this
    delete this.init
    return this
  },
  get_destination(){
    if(db.debug){
      return '_dev.setting'
    }
    else{
      return 'common.setting'
    }
  },
  save_enter(login){
    return con.request(this.get_destination(), '/save_enter', { login })
  },
  soz_user:{
    get_users(login){
      return con.request(this.parent.get_destination(), '/soz_user/get_users', { login })
    },
    delete(soz_login){
      return con.request(this.parent.get_destination(), '/soz_user/delete', { soz_login })
    },
    save(login,soz_login, role, firstname, lastname, middlename, description){
      return con.request(this.parent.get_destination(), '/soz_user/save', { firstname, lastname, middlename, login, role, description, soz_login })
    },
  }
}.init()
