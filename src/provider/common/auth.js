import con from '../../connection'
import db from '../../db'

export default {
  init(){
    this.user.parent = this
    delete this.init
    return this
  },
  get_destination(){
    if(db.debug){
      return '_dev.auth'
    }
    else{
      return 'common.auth'
    }
  },
  user:{
    authorize(login, secret){
      return con.request(this.parent.get_destination(), '/user/authorize', { login, secret })
    },
    detail(login){
      return con.request(this.parent.get_destination(), '/user/detail', { login })
    }
  }
}.init()
