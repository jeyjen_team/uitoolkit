import auth from './common/auth'
import soz_proxy from './soz/proxy'
import generator from './soz/generator'
import soz_utility from './soz/utility'
import soz_monitor from './soz/monitoring'
import mx_proxy from './soz/mx_proxy'
import setting from './common/setting'

export default {
  auth: auth,
  setting: setting,
  soz:{
    proxy: soz_proxy,
    generator: generator,
    utility: soz_utility,
    monitor: soz_monitor,
    mx_proxy: mx_proxy,
  }
}
