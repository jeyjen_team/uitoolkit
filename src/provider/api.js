import con from '../connection'
import db from '../db'
import axios from 'axios'

export default {
  init(){
    return this
  },
  details(idioms){
     return axios.post(`/api/idiom/details`, {idioms, lang: 'ru'})
  },
  text_stats(text){
    return axios.post(`/api/text/stats`, {text, lang: 'en'})
  },
  phrase_generate(pattern, vars){
    return axios.post(`/api/phrase/generate`, {pattern, vars})
  }
}.init()
