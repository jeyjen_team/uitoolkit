import con from '../../connection'

export default {
  init(){
    this.client.parent = this
    delete this.init
    return this
  },

  get_destination(prefix){
    return prefix + ".mx_proxy"
  },
  client:{
    delete_from_buffer(prefix, order_id){
      return con.request(this.parent.get_destination(prefix), '/client/delete_from_buffer', {order_id})
    }
  },
}.init()
