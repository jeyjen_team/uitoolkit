import con from '../../connection'

let sub = null;
export default {
  init(){
    this.order.parent = this
    this.user.parent = this
    this.catalog.parent = this
    delete this.init
    return this
  },
  // init(prefix){
  //   this._destination = prefix + '.' + '@soz.proxy';
  //   this._events_destination = '/topic/' + prefix + '.subject.soz.order';
  //   if(sub != null){
  //     this.unsubscribe();
  //     this.subscribe();
  //   }
  // }
  get_destination(prefix){
    return prefix + ".soz_proxy"
  },

  subscribe(){
    // sub = con.subscribe(this._events_destination, (m)=>{
    //   if(m.context === 'doc_document'){
    //     if(m.type === 'created')
    //       store.dispatch(name.soz.e.order_created, m.body);
    //     else if(m.type === 'updated')
    //       store.dispatch(name.soz.e.order_updated, m.body);
    //   }
    //   else if(m.context === 'doc_processing'){
    //     store.dispatch(name.soz.e.stages_updated, m.body);
    //   }
    //   else if(m.context === 'doc_attribute') {
    //     store.dispatch(name.soz.e.attributes_updated, m.body);
    //   }
    // });
  },
  unsubscribe(){
    // if(sub !== null)
    //   sub.unsubscribe();
    // sub = null;
  },

  v_log(prefix, order_id, level){
    return con.request(this.get_destination(prefix), '/v_log', {order_id, level})
  },
  get_config(prefix, app, name){
    return con.request(this.get_destination(prefix), '/get_config', {app, name})
  },
  set_config(prefix, app, name, value){
    return con.request(this.get_destination(prefix), '/set_config', {app, name, value})
  },
  add_card(prefix, number, add_number, currency, external_id, expiration_date){
    return con.request(this.get_destination(prefix), '/add_card', {number, add_number, currency, external_id, expiration_date})
  },

  order:{
    orders(prefix, type, string1, state, limit, int5, create_by, identifiers, from, to){
      return con.request(this.parent.get_destination(prefix), '/order/orders', {type, limit, string1, state, int5, create_by, identifiers, from, to})
    },
    attributes(prefix, order_id, attributes){
      return con.request(this.parent.get_destination(prefix), '/order/attributes', {order_id, attributes})
    },
    save_order(prefix, attributes){
      return con.request(this.parent.get_destination(prefix), '/order/save_order', {attributes})
    },
    delete_attributes(prefix, order_id, attributes){
      return con.request(this.parent.get_destination(prefix), '/order/delete_attributes', {order_id, attributes})
    },
    stages(prefix, id, order_id){
      return con.request(this.parent.get_destination(prefix), '/order/stages', {id, order_id})
    },
    save_stage(prefix, attributes){
      return con.request(this.parent.get_destination(prefix), '/order/save_stage', {attributes})
    },
    delete_stage(prefix, identifiers){
      return con.request(this.parent.get_destination(prefix), '/order/delete_stage', {identifiers})
    },
    attach_document(prefix, order_id, type){
      return con.request(this.parent.get_destination(prefix), '/order/attach_document', {order_id, type})
    },
    get_attachments(prefix, order_id){
      return con.request(this.parent.get_destination(prefix), '/order/get_attachments', {order_id})
    }
  },
  user:{
    get(prefix, limit, identifiers, logins, query){
      return con.request(this.parent.get_destination(prefix), '/user/get', {limit, identifiers, logins, query})
    },
    get_by_id(prefix, identifiers){
      return con.request(this.parent.get_destination(prefix), '/user/get_by_id', {identifiers})
    },
    get_by_login(prefix, logins){
      return con.request(this.parent.get_destination(prefix), '/user/get_by_login', {logins})
    },
    save_user(prefix, login, role, firstname, lastname, middlename, description){
      return con.request(this.parent.get_destination(prefix), '/user/save_user', {login, role, firstname, lastname, middlename, description})
    }
  },
  catalog:{
    get_sub_operations(prefix, type, status, role){
     return con.request(this.parent.get_destination(prefix), '/catalog/get_sub_operations', {type, status, role})
    }
  },



  // card(args){
  //   /*
  //     {
  //       type: 26,
  //       id: 3088,
  //       status: "all", // {all, active, blocked}
  //       tariff_plan: "Базовый",
  //       tariff: "Базовый Автокарта",
  //       currency: "RUB",
  //       card: "PayPass MasterCard World",
  //       design: "Море и горы",
  //       card_param: "",
  //       action: "3 месяца бесплатного обслуживания",
  //       segment: "Нет",
  //       sys_code: "5103",
  //       sys_group: "183"
  //     }
  //   */
  //   return con.get(this._destination, "product", "card", JSON.stringify(args));
  // },
  // credit(args){
  //   /*
  //   {
  //     sys_code: "3569",
  //     id: 108
  //   }
  //   */
  //   return con.get(this._destination, "product", "credit", JSON.stringify(args));
  // },
  // card_tariff(type){
  //   return con.get(this._destination, "product", "tariff", JSON.stringify({type}));
  // },
  // get_filtered_orders(args){
  //   /*
  //    {
  //      "limit": 10,
  //      "create_by": 123
  //      "type": 26,
  //      "string1": "asd"
  //    }
  //   * */
  //   return con.get(this._destination, "order", "filtered", JSON.stringify(args));
  // },
  // get_orders(orders){
  //   /*
  //   {
  //       "identifiers": [12123, 12123123]
  //   }
  //   */
  //   return con.get(this._destination, "order", "", JSON.stringify({identifiers: orders}));
  // },
  // get_attributes(args){
  //   /*
  //    {
  //       "order_id": 12123,
  //       "attributes": ["attr1", "attr2"]
  //    }
  //    */
  //   return con.get(this._destination, "order", "attributes", JSON.stringify(args));
  // },
  // get_stages(args){
  //   /*
  //    {
  //       "order_id": 12123,
  //       "full": true
  //    }
  //    */
  //   return con.get(this._destination, "order", "stages", JSON.stringify(args));
  // },
  //
  // save_order(attrs){
  //   let req = { attributes: attrs };
  //   return con.post(this._destination, "order", "", JSON.stringify(req));
  // },
  // save_stage(attrs){
  //   let req = { attributes: attrs }
  //   return con.post(this._destination, "order", "stage", JSON.stringify(req));
  // },
  // get_users(logins){
  //   return con.get(this._destination, "user", "", JSON.stringify({logins}));
  // },
  // save_user(login, role, firstname, lastname, middlename, description){
  //   return con.post(this._destination, "user", "save", JSON.stringify({login, role, firstname, lastname, middlename, description}))
  // },
  // get_roles(){
  //   return con.get(this._destination, "catalog", "role", '');
  // }
}.init()
