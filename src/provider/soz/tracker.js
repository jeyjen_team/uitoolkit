import con from '../../connection'
import eve from '../../event'

let sub = null
let prefix = ''
export default {
  init(){
    eve.v.main.contour((e)=>{
      prefix = e.name
      this.act_subscription()
    })
    eve.on('event.pvd.tracker.order_created', this.act_subscription.bind(this))
    eve.off('event.pvd.tracker.order_created', this.act_subscription.bind(this))
    eve.on('event.pvd.tracker.order_changed', this.act_subscription.bind(this))
    eve.off('event.pvd.tracker.order_changed', this.act_subscription.bind(this))
    eve.on('event.pvd.tracker.stage_changed', this.act_subscription.bind(this))
    eve.off('event.pvd.tracker.stage_changed', this.act_subscription.bind(this))

    delete this.init
    return this
  },
  get_destination(){
    return `/topic/${prefix}.soz_tracker`
  },
  act_subscription(){
    let oc = eve.count('event.pvd.tracker.order_changed')
    let sc = eve.count('event.pvd.tracker.stage_changed')
    let c =  oc + sc
    if(c > 0 && prefix.length > 0){
      this.connect(prefix)
    }
    else{
      if(sub != null)
        sub.unsubscribe();
      sub = null;
    }
  },
  connect(prefix){
    if(sub != null)
      sub.unsubscribe();
    sub = null;
    sub = con.subscribe(this.get_destination(prefix), (m)=>{
      if(m.context == 'stage'){
        eve.pvd.tracker.stage_changed(m.body.order_id, m.body.count)
      }
      else if(m.context == 'order'){
        if(m.name == 'new'){
          eve.pvd.tracker.order_created(m.body.order_id, m.body.attributes)
        }
        else{
          eve.pvd.tracker.order_changed(m.body.order_id, m.body.attributes)
        }
      }
    })
  },
}.init()
