import con from '../../connection';
class generator
{
  constructor(){
    this._destination = '';
  }
  init(prefix){
    this._destination = prefix + '.' + '@soz.generator';
  }
  generate(type, args){
    /*
     {
     sex: "male", // {male, female}
     age: 30,
     login: 'crd_nov',
     income: 50000,
     sas: "Default=2"
     id: 3088,
     sys_code: "5103",
     attributes: {
     Client_FirstName: "Азат"
     }

     *** ДЛЯ КАРТ ***

     tariff_plan: "Базовый",
     tariff: "Базовый Автокарта",
     currency: "RUB",
     card: "PayPass MasterCard World",
     design: "Море и горы",
     card_param: "",
     action: "3 месяца бесплатного обслуживания",
     segment: "Нет",

     sys_group: "183",
     }
     */
    let action = '';
    if(type == '26')
      action = 'debit_card'
    else if(type == '23')
      action = 'credit_card'
    else if(type == '13')
      action = 'credit'

    return con.get(this._destination, '', action, JSON.stringify(args));
  }
}

export default new generator();

