
import con from '../../connection'
import db from '../../db'

export default {
  init(){
    delete this.init
    return this
  },
  get_destination(){
    if(db.debug){
      return '_dev.soz_utility'
    }
    else{
      return 'common.soz_utility'
    }
  },
  copy(from, to, order_id){
    return con.request(this.get_destination(), '/copy', {from, to, order_id})
  },
}.init()
