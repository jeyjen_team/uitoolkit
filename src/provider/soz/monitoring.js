import con from '../../connection'

export default {
  init(){
    this.db.parent = this
    this.service.parent = this
    delete this.init
    return this
  },
  get_destination(prefix){
    return `${prefix}.soz_monitoring`
  },
  get_event_destination(prefix){
    return `/topic/${prefix}.soz_monitoring`
  },
  subscribe(prefix, callback){
    return con.subscribe(this.get_event_destination(prefix), callback);
  },
  unsubscribe(sub){
    if(sub !== null)
      sub.unsubscribe();
  },

  db:{
    check(prefix){
      return con.request(this.parent.get_destination(prefix), '/db/check', {})
    },
    sas(prefix, key){
      return con.request(this.parent.get_destination(prefix), '/db/sas', {key})
    },
    set_sas(prefix, key, value){
      return con.request(this.parent.get_destination(prefix), '/db/set_sas', {key, value})
    },
  },
  service:{
    get(prefix){
      return con.request(this.parent.get_destination(prefix), '/service/get', {})
    },
    start(prefix, name){
      return con.request(this.parent.get_destination(prefix), '/service/start', {name})
    },
    stop(prefix, name){
      return con.request(this.parent.get_destination(prefix), '/service/stop', {name})
    },
  }
}.init()
