/*
let save = (key, value)=>{
  localStorage.setItem(key, JSON.stringify({value:value}));
};
let retrieve = (key, def)=>{
  let value = localStorage.getItem(key);
  if(value !== null && value !== undefined)
  {
    return JSON.parse(value).value;
  }
  return def;
};
let remove_item = (key)=>{
  localStorage.removeItem(key);
};

 let db_version = "2.0"

let prepare = function(prefix, o){
  let res = {}
  for(let k in o){
    let key = `${prefix}.${k}`
    if (o[k] !== null && typeof o[k] === 'object' && !Array.isArray(o[k]) ) {
      res[k] = prepare(key, o[k]);
    }
    else{
      Object.defineProperty(res, k, {
        get: function() { return retrieve(key, o[k]) },
        set: function(v) { save(key, v) },
        enumerable: true,
        configurable: true
      })
    }
  }
  return res
}

let debug = process.env.NODE_ENV !== 'production'

let obj = {
  debug: debug,
  version: '',
  ui_version:'',
  layout: null,
  contour: '',
  user:{
    login: '',
    firstname: '',
    lastname: '',
    middlename: '',
    email: '',
    login_time: ''
  },
  view:{
    order:{
      query: '',
      tab: null,
      order_id: ''
    },
    order_list:{
      limit: '10',
      type: '',
      string1: '',
      create_by: '',
      state: '',
      from: '',
      to: ''
    },
    xerox:{
      from: '',
      to: '',
      cloned_list:[] // {origin_id, copy_id}
    },
    soz_users:{
      query: '',
      limit: 5,
      only_my: true
    }
  }
}
obj = prepare('_', obj)


let v = obj.version
if(v !== db_version){
  localStorage.clear()
  obj.version = db_version
}

export default obj

*/
let debug = process.env.NODE_ENV !== 'production'
let db_version = "2.0"

function save(key, value){
  localStorage.setItem(key, JSON.stringify({value:value}));
};
function retrieve(key){
  let value = localStorage.getItem(key);
  if(value !== null && value !== undefined){
    return JSON.parse(value).value;
  }
  return null;
};
function get_param_names(func){
  let r = func()
  let params = []
  if(r) {
    for(let k in r) {
      params.push(k)
    }
  }
  return params
}
function prepare(prefix, obj){
  let stack = [{prefix: prefix, obj: obj}]
  while(stack.length > 0){
    let c = stack.pop()
    let prefix = c.prefix
    let o = c.obj
    for(let k in o){
      if (o[k] === null)
        continue
      let t = typeof o[k]
      let name = `${prefix}.${k}`
      if(t === 'object'){
        stack.push({prefix: name, obj: o[k]})
      }
      else if(t === 'function'){
        let params = get_param_names(o[k])
        o[k] = function () {
          let as = arguments
          let result = null;

          if(as.length > 0){
            // сохранить значение в БД
            // формирование объекта для сохранения
            if(params.length == 1){
              save(name, arguments[0])
            }
            else{
              let min_length = params.length < arguments.length? params.length: arguments.length
              let data = {}
              for(let i = 0; i < min_length; i++){
                data[params[i]] = arguments[i]
              }
              // заполнение не указанных значений
              for(let i = 0; i < params.length - arguments.length; i++){
                let idx = min_length + i;
                data[params[idx]] = null
              }
              save(name, data)
            }
          }
          else{
            result = retrieve(name)
            if(params.length > 1 && result == null){
              return {}
            }
          }
          return result
        }
      }
    }
  }
}

let state = {
  version(value){return {value}},
  ui_version(value){return {value}},
  layout(obj){return {obj}},
  contour(value){return {value}},
  user(login, firstname, lastname, middlename, email){return {login, firstname, lastname, middlename, email}},
  login_time(value){return {value}},
  view:{
    phrases:{
      theme(value){return {value}}
    },
    order:{
      query(value){return {value}},
      tab(value){return {value}},
      order_id(value){return {value}}
    },
    order_list:{
      limit(value){return {value}},
      type(value){return {value}},
      string1(value){return {value}},
      create_by(value){return {value}},
      state(value){return {value}},
      from(value){return {value}},
      to(value){return {value}}
    },
    xerox:{
      from(value){return {value}},
      to(value){return {value}},
      cloned_list(value){return {value}}
    },
    soz_users:{
      query(value){return {value}},
      limit(value){return {value}}, // 5
      only_my(value){return {value}} // true
    }
  }
}
prepare('_', state)

let v = state.version()
if(v !== db_version){
  localStorage.clear()
  state.version(db_version)
}
state[debug] = debug
export default state
