export default [
  // code
  {
    _id: '_design/my_index',
    views: {
      by_name: {
        map: function (doc) {
          if(doc.type == 'phrase'){
            doc.words.forEach((it)=>{
              emit([doc.prj_id, it])
            })
          }
        }.toString(),
        reduce: '_count'
      }
    }
  },

  // data
  {
    _id: 'prj_1',
    type: 'prj',
    name: 'английский язык',
    desc: 'desc1'
  },
  {
    _id: 'prj_2',
    type: 'prj',
    name: 'бизнес',
    desc: 'desc1'
  },
  // phrases
  {
    _id: 'ph_1',
    type: 'phrase',
    prj_id: 'prj_1',
    phrase: 'computer is the best',
    score: 10,
    words: ['computer', 'is', 'the', 'best'],
    tags:['info', 'info2', 'tag2'],
  },
  {
    _id: 'ph_2',
    type: 'phrase',
    prj_id: 'prj_1',
    phrase: 'computer is the best all',
    score: 20,
    words: ['computer', 'is', 'the', 'best', 'all'],
    tags:['info', 'info2', 'tag1']
  },
  {
    _id: 'ph_3',
    type: 'phrase',
    prj_id: 'prj_1',
    phrase: 'computer is the text data',
    score: 20,
    words: ['computer', 'is', 'the', 'text', 'data'],
    tags:['info', 'info2']
  }
]
