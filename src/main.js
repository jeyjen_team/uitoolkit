//import "babel-polyfill"
// import {polyfill} from 'es6-promise';// Подключение объектов Promise для IE
// polyfill();
import 'vue2-scrollbar/dist/style/vue2-scrollbar.css'
import 'vue2-scrollbar/dist/style/app.css'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/fontawesome-all.min.css'
import './assets/style.css'
import Vue from 'vue'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/ru-RU'
import moment from 'moment'
import momentDurationFormatSetup from 'moment-duration-format'
//import con from './connection'
import app from './app'
import eve from './event'
import db from './pouch'
//import async_computed from 'vue-async-computed'
db.open()

// https://pouchdb.com/guides/queries.html

window.onbeforeunload = function(){
  eve.app.close()
}
eve.app.start()

momentDurationFormatSetup(moment);
Vue.config.productionTip = false
Vue.config.silent = true;
Vue.use(ElementUI, { size: 'mini' , locale: locale})
//Vue.use(async_computed)
//Vue.prototype.$db = db
// cto-novikov-eo - 10.81.70.240:61614
// 511            - 10.48.32.154:61614
// 115            - 10.48.42.96:61614

new Vue({
  el: '#app',
  template: '<app/>',
  components: {
    app: app
  }
})

//con.open();
console.log('ACTIVEMQ_HOST: ' + ACTIVEMQ_HOST);
console.log('VERSION: ' + VERSION);

// инициализация приложения


//process.env.NODE_ENV = 'dev'

