import Stomp from 'stompjs';
import uuid from 'uuid';
import eve from './event';

// добавить name

let host = 'ws://' + ACTIVEMQ_HOST;
let client = null;
let listen_queue = '/temp-queue/ui_client_' + uuid.v4();
let sub = null;
let is_alive = false;
let is_connecting = false;
let timer_id = null;

export default {
  _request: new Map(),
  e: {
      on_open:'on_open',
      on_close:'on_close'
    },
  init(){
    this._on_responce = this._on_responce.bind(this)
    return this
  },
  _on_responce(message){
    try
    {
      let id = message.headers['correlation-id'];
      if (this._request.has(id)) {
        let handler = this._request.get(id);
        let body = null;
        if (message.headers['result'] === 'success') {
          if (message.body.length > 0) // проверить на наличие null в body
          {
            body = JSON.parse(message.body);
          }
          handler.resolve_func(body);
        }
        else if (message.headers['result'] === 'error') {
          if (message.body.length > 0) {
            try {
              body = JSON.parse(message.body);
            }
            catch (e) {
            }
          }
          handler.reject_func(body.message);
        }
      }
      else {
      }
    }
    catch (e){
    }
  },
  open(){
    let con = this
    timer_id = setInterval(()=>{
      if(! is_alive && !is_connecting){
        is_connecting = true;
        client = Stomp.client(host);
        client.maxWebSocketFrameSize = 51200;
        client.debug = function(str) {
          //console.log(str)
        };
        client.connect('', '', (frame)=>
          {
            sub = client.subscribe(listen_queue, con._on_responce);
            is_alive = true
            is_connecting = false
            eve.con.connected()
          },
          (f)=>
          {
            is_alive = false
            is_connecting = false
            eve.con.disconnected()
          });
      }
    }, 2000);
  },
  close(){
    clearInterval(timer_id);
    client.disconnect();
    emit.connection._close();
  },
  request(destination, action, body){
    let con = this
    return new Promise((resolve_func, reject_func)=>
    {
      let id = uuid.v4();
      con._request.set(id, {resolve_func: resolve_func, reject_func: reject_func});
      let h = {};
      h['action'] = action;
      h['correlation-id'] = id;
      h['reply-to'] = listen_queue;
      h['type'] = 'request';
      client.send(destination, h, JSON.stringify(body));
    });
  },
  subscribe(destination, callback){
    let sub = client.subscribe(destination, (m)=> {
      let body = null;
      try
      {
        body = JSON.parse(m.body);
      }catch(e)
      {
        body = m.body;
      }

      let e =
        {
          context: m.headers.context,
          name: m.headers.name,
          type: m.headers.type,
          body: body
        };
      callback(e);
    });

    return sub;
  }
  // get(destination, context, action, body){
  //   return this.request('get', destination, context, action, body)
  // }
  // post(destination, context, action, body){
  //   return this.request('post', destination, context, action, body)
  // }
  // put(destination, context, action, body){
  //   return this.request('put', destination, context, action, body)
  // }
  // delete(destination, context, action, body){
  //   return this.request('delete', destination, context, action, body)
  // }

  // on_open(callback){
  //   this.on(this.e.on_open, callback);
  // }
  // on_close(callback){
  //   this.on(this.e.on_close, callback);
  // }
}.init()
