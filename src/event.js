import Vue from 'vue'
const event_bus = new Vue()

let count={}
let _count = function(ent){
  return count[ent]
}
let _on = function(name, callback){
  let n = `_on.${name}`
  if(callback){
    event_bus.$on(n, callback)
  }
  else{
    event_bus.$emit(n)
  }
}
let _off = function(name, callback){
  let n = `_off.${name}`
  if(callback){
    event_bus.$off(n, callback)
  }
  else{
    event_bus.$emit(n)
  }
}

function get_param_names(func) {
  let r = func()
  let params = []
  if(r) {
    for(let k in r) {
      params.push(k)
    }
  }
  return params
}
// подготовка методов оповещений
let prepare = function(prefix, obj){

  let stack = [{prefix: prefix, obj: obj}]
  while(stack.length > 0){

    let c = stack.pop()
    let prefix = c.prefix
    let o = c.obj

    for(let k in o){
      if (o[k] === null)
        continue
      let t = typeof o[k]
      let name = `${prefix}.${k}`
      if(t === 'object'){
        stack.push({prefix: name, obj: o[k]})
      }
      else if(t === 'function'){
        let params = get_param_names(o[k])

        count[name] = 0
        o[k] = function () {
          let as = arguments
          if(as.length > 0 && typeof as[0] === 'function' ){
            if(as.length > 1 && as[1] === false){
              // отписка от событий
              event_bus.$off(name, as[0])
              count[name]--
              _off(name)
              console.log(`off event ${name}`)
            }
            else{
              // подписка на событие
              event_bus.$on(name, as[0])
              count[name]++
              _on(name)
              console.log(`on event ${name}`)
            }
          }
          else{
            let min_length = params.length < arguments.length? params.length: arguments.length
            let e = {}
            for(let i = 0; i < min_length; i++){
              e[params[i]] = arguments[i]
            }

            for(let i = 0; i < params.length - arguments.length; i++){
              let idx = min_length + i;
              e[params[idx]] = null
            }

            event_bus.$emit(name, e)
            console.log('event: ' + name)
          }
          return count
        }
      }
    }

  }
  obj['on'] = _on
  obj['off'] = _off
  obj['count'] = _count
}

let s = {
  app:{
    start(){return {}},
    close(){return {}},
    notify:{
      info(title, message){return {title, message}},
      success(title, message){return {title, message}},
      warn(title, message){return {title, message}},
      error(title, message){return {title, message}},
    },
    login(name){return {name}},
    logout(){return {}}
  },
  db:{
    opened(){return {}},
    closed(){return {}}
  },
  con:{
    connected(){return {}},
    disconnected(){return {}}
  },
  v:{ // view
    main:{
      contour(name){return {name}}
    },
    order_list:{
      selected(id){return {id}}
    }
  },
  pvd:{ // provider
    monitoring:{
      db:{
        state(value){return {value}},
        sas(value){return {value}},
      },
      service(name, status){return {name, status}}
    },
    order:{
      selected(id){return {id}},
    },
    tracker:{
      order_changed(order_id, state){ return {order_id, state} },
      order_created(order_id, state){ return {order_id, state} },
      stage_changed(order_id, count){ return {order_id, count} },
    }
  }
}
prepare('event', s)
export default s
