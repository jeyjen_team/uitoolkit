export default function (interval, callback, immeditaly) {
  let il = interval
  if(immeditaly){
    callback()
  }

  let rep = function () {
    if(il > 0){
      callback()
      setTimeout(rep, il)
    }
  }
  rep()
  let cancel = function () {
    il = -1
  }

  return cancel
}
